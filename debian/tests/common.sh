#!/bin/sh

set -e

test_this() {
	echo
	echo "running $@"
	$@
}

create_packages () {
    # set up a very simple test package

    mkdir -p t/DEBIAN t/usr t/etc
    cat >t/DEBIAN/control <<EOF
Package: t
Version: 4
Maintainer: Piu Parts <piuparts-devel@alioth-lists.debian.net>
Priority: optional
Architecture: all
Installed-Size: 0
Description: Auto Package Test Dummy
 Extremely simple binary package for piuparts testing
EOF

    dpkg-deb -b t

    rm -r t/

    # another simple package, but set up for failure

    mkdir -p f/DEBIAN f/usr f/etc
    cat >f/DEBIAN/control <<EOF
Package: f
Version: 4
Maintainer: Piu Parts <piuparts-devel@alioth-lists.debian.net>
Priority: optional
Architecture: all
Installed-Size: 0
Description: Auto Package Test Fail Dummy
 Extremely simple binary package for piuparts testing - fail version
EOF

    cat >f/DEBIAN/postinst <<EOF
#! /bin/sh
mkdir -p /etc/f/
touch /etc/f/ailure
EOF

    chmod +x f/DEBIAN/postinst

    dpkg-deb -b f

    rm -r f/
}
